package com.nearsoft.javaschool.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private UserService userService;

    private HttpHeaders requestHeaders;
    private HttpEntity<User> data;

    @Test
    public void testAddUser() {
        requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        String addName = "other_name";
        String addMail = "other.mail@mail.com";
        String addPassword = "other_password";
        User user = new User(addName, addMail, addPassword);
        data = new HttpEntity<>(user, requestHeaders);
        ResponseEntity<User> responseEntityUser = testRestTemplate.postForEntity("/api/v1/users/register", data, User.class);
        User responseUser = responseEntityUser.getBody();
        assertThat(responseUser).isNotNull();
        assertThat(responseUser.getId()).isNotNull();
        assertThat(responseUser.getEmail()).isEqualTo(addMail);
        assertThat(responseUser.getPassword()).isNull();
        assertThat(responseUser.getName()).isEqualTo(addName);
    }

    @Test
    public void testAddUserExists() throws Exception {
        requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        String addName = "other_name";
        String addMail = "email@email.com";
        String addPassword = "other_password";
        User user = new User(addName, addMail, addPassword);
        userService.addUser(user);
        data = new HttpEntity<>(user, requestHeaders);
        ResponseEntity<User> responseEntityUser = testRestTemplate.postForEntity("/api/v1/users/register", data, User.class);
        HttpStatus httpStatus = responseEntityUser.getStatusCode();
        assertThat(httpStatus).isEqualTo(HttpStatus.BAD_REQUEST);
    }
}