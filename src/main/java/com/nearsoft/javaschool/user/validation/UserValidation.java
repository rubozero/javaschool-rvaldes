package com.nearsoft.javaschool.user.validation;

import com.nearsoft.javaschool.user.User;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.nearsoft.javaschool.user.validation.ValidationResult.invalid;
import static com.nearsoft.javaschool.user.validation.ValidationResult.valid;

public interface UserValidation extends Function<User, ValidationResult> {
    String INVALID_USER = "Invalid User";
    String INVALID_EMAIL_ADDRESS = "Invalid email address";

    static UserValidation userIsNotNull() {
        return holds(user -> user != null, INVALID_USER);
    }

    static UserValidation nameIsNotEmpty() {
        return holds(user -> StringUtils.isNotBlank(user.getName()), INVALID_USER);
    }

    static UserValidation emailIsNotEmpty() {
        return holds(user -> StringUtils.isNotBlank(user.getEmail()), INVALID_USER);
    }

    static UserValidation passwordIsNotEmpty() {
        return holds(user -> StringUtils.isNotBlank(user.getPassword()), INVALID_USER);
    }

    static UserValidation emailValidFormat() {
        return holds(user -> EmailValidator.getInstance().isValid(user.getEmail()), INVALID_EMAIL_ADDRESS);
    }

    static UserValidation holds(Predicate<User> p, String message) {
        return user -> p.test(user) ? valid() : invalid(message);
    }

    default UserValidation and(UserValidation other) {
        return user -> {
            final ValidationResult result = this.apply(user);
            return result.isValid() ? other.apply(user) : result;
        };
    }

    static UserValidation all(UserValidation... validations) {
        return user -> {
            String reasons = Arrays.stream(validations)
                    .map(v -> v.apply(user))
                    .filter(r -> !r.isValid())
                    .map(r -> r.getReason().get())
                    .limit(1)
                    .collect(Collectors.joining(", "));
            return reasons.isEmpty() ? valid() : invalid(reasons);
        };
    }
}
