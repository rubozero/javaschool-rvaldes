package com.nearsoft.javaschool.user.validation;


import java.util.Optional;

public class Invalid implements ValidationResult {
    private final String reason;

    Invalid(String reason) {
        this.reason = reason;
    }

    public boolean isValid() {
        return false;
    }

    public Optional<String> getReason() {
        return Optional.of(reason);
    }
}
