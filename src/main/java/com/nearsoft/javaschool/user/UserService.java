package com.nearsoft.javaschool.user;

import com.nearsoft.javaschool.Exception.InvalidUserException;
import com.nearsoft.javaschool.user.validation.UserValidation;
import com.nearsoft.javaschool.user.validation.ValidationResult;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static com.nearsoft.javaschool.user.validation.UserValidation.*;

@Service
public class UserService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;

    private final String USER_EXISTS = "User already exists";

    public UserService(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User addUser(User user) throws InvalidUserException {
        User processedUser = processUser(user);
        User returnUser = userRepository.save(processedUser);
        returnUser.setPassword(null);
        return returnUser;
    }

    private User processUser(User user) throws InvalidUserException {
        validUser(user);
        validateEmailExists(user);
        user.setPassword(passwordEncrypt(user.getPassword()));
        return user;
    }

    private void validUser(User user) throws InvalidUserException {
        UserValidation userValidation =
                all(userIsNotNull(), nameIsNotEmpty(), emailIsNotEmpty(), emailValidFormat(), passwordIsNotEmpty());
        ValidationResult validationResult = userValidation.apply(user);
        if (validationResult.getReason().isPresent()) {
            throw new InvalidUserException(validationResult.getReason().get());
        }
    }

    private void validateEmailExists(User user) throws InvalidUserException {
        User userExists = userRepository.findByEmail(user.getEmail());
        if (userExists != null) {
            throw new InvalidUserException(USER_EXISTS);
        }
    }

    private String passwordEncrypt(String password) {
        return passwordEncoder.encode(password);
    }
}
