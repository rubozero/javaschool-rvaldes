package com.nearsoft.javaschool.Exception;

public class InvalidUserException extends Exception {

    public InvalidUserException(String message) {
        super(message);
    }
}
